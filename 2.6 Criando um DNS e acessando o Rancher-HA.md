### No node extras
Instalar o Bind
```bash
sudo apt update && sudo apt install -y bind9
```

Acessar o diretório de configurações
```bash
cd /etc/bind
```

`sudo vim named.conf.local`
```c
// zona direta
zone "exemplo-ha.com" {
    type master;
    file "/etc/bind/db.exemplo-ha.com";
};

// zona reversa
zone "192.-in.addr.arpa" {
    type master;
    file "/etc/bind/db.192";
};
```

`sudo vim db.exemplo-ha.com`
```conf
$TTL    604800
@       IN      SOA     exemplo-ha.com. root.exemplo-ha.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      exemplo-ha.com.
@       IN      A       192.168.1.204
rancher1    IN      A       192.168.1.201
rancher2    IN      A       192.168.1.202
rancher3    IN      A       192.168.1.203
rancher4    IN      A       192.168.1.204
rancher-ha    IN      A       192.168.1.201
*.rancher-ha IN      A       192.168.1.201
*.rancher-ha IN      A       192.168.1.202
*.rancher-ha IN      A       192.168.1.203
```

`sudo vim db.192`
```conf
$TTL    604800
@       IN      SOA     exemplo-ha.com. root.exemplo-ha.com. (
                              1         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      exemplo-ha.com.
204.1.168       IN      PTR     exemplo-ha.com.
```

Reiniciar e ver o status do bind
```bash
sudo systemctl restart bind9 && \
sudo systemctl status bind9
```

### Em todos os nodes

Configurar o resolvconf
`sudo vim /etc/netplan/00-installer-config.yaml`
```conf
      nameservers:
        addresses: [192.168.1.204,8.8.8.8]
```

Aplicar a configuração:
```
sudo netplan apply
```

Testar os dominios
```bash
host exemplo-ha.com
nslookup rancher.exemplo-ha.com
```

Fonte:
- Bind - https://www.youtube.com/watch?v=PE4ejgbZYRo
- Resolvconf - https://linuxhint.com/update-resolv-conf-on-ubuntu/
