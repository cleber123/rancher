**Executar nos nodes Rancher como root**
```bash
curl https://releases.rancher.com/install-docker/20.10.sh | sh && \
systemctl enable docker  && \
 usermod -aG docker ubuntu
```

**Executar no node extras**
Instalar Kubectl
```bash
sudo curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
sudo chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```
